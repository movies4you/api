const functions = require('firebase-functions');
const admin = require('firebase-admin');
const { google } = require('googleapis');

admin.initializeApp(functions.config().firebase);

const db = admin.firestore();
db.settings({ timestampsInSnapshots: true});

createUser = async (user) => {
  const userName = user.email.split("@")[0];
  const data = {email: user.email, name: userName};
  const usersRef = db.collection('users');
  const reviewsRef = usersRef.doc(user.uid).collection('reviews');
  const moviesRef = db.collection('movies');
  const randomId = Math.floor(Math.random() * 100000) + 1;

  await usersRef.doc(user.uid).set(data);

  let batch = db.batch();
  const moviesSnapshot = await moviesRef.where('movieId', '>', randomId).limit(20).get();
  if (moviesSnapshot.docs && moviesSnapshot.docs.length > 0) {
    moviesSnapshot.forEach(document => batch.set(reviewsRef.doc(), document.data()));
  }

  await batch.commit();
};

deleteUser = (user) => {
  return db.collection('users').doc(user.uid).delete();
};

rateMovie = async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called while authenticated.');
  }

  const userId = context.auth.uid;
  const movieId = data.movieId;
  const genres = data.genres;
  const title = data.title;
  const rating = data.rating;
  const timestamp = new Date().getTime() / 1000;

  const usersRef = db.collection('users');
  const reviewsRef = usersRef.doc(userId).collection('reviews');

  // Get all user ratings
  const ratings = [];
  const ratingsRef = db.collection('users').doc(userId).collection('ratings');
  const ratingsSnapshot = await ratingsRef.get();
  if (ratingsSnapshot.docs && ratingsSnapshot.docs.length > 0) {
    ratingsSnapshot.forEach(document => { ratings.push(document.data()) });
  }
  console.log(`Found ${ratings.length} ratings for user ${userId}`);

  // Add movie to user's ratings history
  await ratingsRef.add({ movieId: movieId, rating: rating, title: title, genres: genres, timestamp: timestamp });

  // Delete movie from user's reviews
  const reviewsSnapshot = await reviewsRef.where('movieId', '==', movieId).get();
  if (reviewsSnapshot.docs && reviewsSnapshot.docs.length > 0) {
    reviewsSnapshot.forEach(document => document.ref.delete());
  }

  // Get all user's reviews
  let reviews = [];
  const reviewsFullSnapshot = await reviewsRef.get();
  if (reviewsFullSnapshot.docs && reviewsSnapshot.docs.length > 0) {
    reviewsFullSnapshot.forEach(document => reviews.push(document.data()));
  }

  // Get random movie and add it to user's reviews
  const moviesRef = db.collection('movies');
  let movies = [];
  const moviesSnapshot = await moviesRef.limit(ratings.length + 10).get();
  if (moviesSnapshot.docs && moviesSnapshot.docs.length > 0) {
    moviesSnapshot.forEach(document => { movies.push(document.data()) });
    movies = movies
      .filter(movie => { return ratings.filter(item => { return item.movieId === movie.movieId }).length === 0 })
      .filter(movie => { return reviews.filter(item => { return item.movieId === movie.movieId }).length === 0 })
  }

  console.log(`${movies.length} new reviews available`, movies);
  await reviewsRef.add(movies[0]);
};

moviesList = async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called while authenticated.');
  }

  const userId = context.auth.uid;

  // Get all user ratings
  const ratings = [];
  const ratingsRef = db.collection('users').doc(userId).collection('ratings');
  const ratingsSnapshot = await ratingsRef.get();
  if (ratingsSnapshot.docs && ratingsSnapshot.docs.length > 0) {
    ratingsSnapshot.forEach(document => { ratings.push(document.data()) });
  }
  console.log(`Found ${ratings.length} ratings for user ${userId}`);

  // Get all movies
  let movies = [];
  const moviesRef = db.collection('movies');
  const moviesSnapshot = await moviesRef.get();
  if (moviesSnapshot.docs && moviesSnapshot.docs.length > 0) {
    moviesSnapshot.forEach(document => { movies.push(document.data()) });
  }
  console.log(`Found ${movies.length} movies`);

  // Get 50 random movies that user has not already rated
  const result = movies
    .filter((movie) => { return ratings.filter(item => { return item.movieId === movie.movieId }).length === 0 })
    .sort(() => { return 0.5 - Math.random() })
    .slice(0, 50);

  console.log(`Found ${result.length} matching movies`);
  return {result: result};
};

moviesSuggestions = async (snapshot, context) => {
  // Initialize settings
  const userId = context.params.userId;
  const client = await google.auth.getClient({scopes: ['https://www.googleapis.com/auth/cloud-platform']});
  const ml = google.ml({version: 'v1', auth: client});

  // Get all user ratings
  const instances = [];
  const ratingsRef = db.collection('users').doc(userId).collection('ratings');
  const ratings = await ratingsRef.get();
  if (ratings.docs && ratings.docs.length > 0) {
    ratings.forEach(document => {
      const rating = document.data();
      instances.push([1, rating.movieId, rating.timestamp])
    });
  }
  console.log(instances);

  // Batch delete existing suggestions
  const batchDelete = db.batch();
  const suggestionsRef = db.collection('users').doc(userId).collection('suggestions');
  const suggestions = await suggestionsRef.get();
  if (suggestions.docs && suggestions.docs.length > 0) {
    suggestions.forEach(document => batchDelete.delete(document.ref));
  }

  // Get predictions
  const predictions = await ml.projects.predict({name: `projects/movies4you/models/movies_4_you`, resource: {instances: instances}});
  const results = predictions.data;
  console.log(results);

  // Batch add suggestions to database
  const batchAdd = db.batch();
  if (results.predictions && results.predictions.length > 0) {
    const firstResults = results.predictions.slice(0, 50);
    firstResults.forEach(prediction => batchAdd.set(suggestionsRef.doc(), {rating: prediction}));
  }

  // Execute both batches
  await batchDelete.commit()
    .then(() => console.log(`[SUCCESS] Old suggestions deletion`))
    .catch(error => console.error(`[ERROR] Old suggestions deletion: `, error));

  await batchAdd.commit()
    .then(() => console.log(`[SUCCESS] New suggestions addition`))
    .catch(error => console.error(`[ERROR] New suggestions addition: `, error));
};

exports.createUser = functions.auth.user().onCreate(createUser);
exports.deleteUser = functions.auth.user().onDelete(deleteUser);
exports.rateMovie = functions.https.onCall(rateMovie);
exports.moviesSuggestions = functions.firestore.document('users/{userId}/ratings/{ratingId}').onCreate(moviesSuggestions);

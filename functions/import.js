const admin = require('firebase-admin');
const serviceAccount = require("./service-account.json");
const data = require("./data.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://movies4you.firebaseio.com"
});

const db = admin.firestore();
db.settings({ timestampsInSnapshots: true});
const moviesRef = db.collection('movies');
const batches = [];

let i,j,temp,chunk = 400;
for (i = 0, j = data.length; i < j; i += chunk) {
  temp = data.slice(i,i+chunk);

  let batch = db.batch();
  temp.forEach(movie => {
    const ref = moviesRef.doc();
    batch.set(ref, {movieId: movie.movieId, title: movie.title, genres: movie.genres});
  });
  batches.push(batch);
}

console.log(`${batches.length} batches created`);

batches.forEach((batch, index) => {
  batch.commit()
    .then(() => {return console.log(`Batch ${index} success`)})
    .catch((error) => console.error(`Batch ${index} error`, error));
});